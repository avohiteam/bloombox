﻿/// Logo Fade out Effect
$(document).ready(function () {
    $(".splash").click(function () {
        $(this).fadeOut();
        return false;
    });

});
/// Logo Fade out Effect End
/// Body Fade IN Effect
$(document).ready(function () {
    $("body").css("display", "none");

    $("body").fadeIn(1000);

    $("a.transition").click(function (event) {
        event.preventDefault();
        linkLocation = this.href;
        $("body").fadeOut(1000, redirectPage);
    });

    function redirectPage() {
        window.location = linkLocation;
    }
});
/// Body Fade IN Effect End
/// Disable Right Click Script
function IE(e) {
    if (navigator.appName == "Microsoft Internet Explorer" && (event.button == "2" || event.button == "3")) {
        return false;
    }
}
function NS(e) {
    if (document.layers || (document.getElementById && !document.all)) {
        if (e.which == "2" || e.which == "3") {
            return false;
        }
    }
}
document.onmousedown = IE; document.onmouseup = NS; document.oncontextmenu = new Function("return false");
/// Disable Right Click Script End
///Mousewheel Scroll 

//$(document).ready(function () {

//    $('html, body, *').mousewheel(function (e, delta) {
//        this.scrollLeft -= (delta * 40);
//        e.preventDefault();
//    });

//});
///Mousewheel Scroll End
///On mouseouer Body Scroll 
var UpdateInterval = 40;
var PixelPerInterval = 10;
var scorllerInterval;

function start_scroll_right() {
    scorllerInterval = setInterval(scroll_right, UpdateInterval);
}

function scroll_right() {
    document.getElementById('scroller').scrollLeft += PixelPerInterval;
}

function stop_scrolling() {
    clearInterval(scorllerInterval);
}
///On mouseouer Body Scroll End
///On mouseouer Img change effect 
$(document).ready(function () {

    $("img.a").hover(
function () {
    $(this).stop().animate({ "opacity": "0" }, "slow");
},
function () {
    $(this).stop().animate({ "opacity": "1" }, "slow");
});

});
///On mouseouer Img change effect End


