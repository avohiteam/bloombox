function body() {
    $('#overlay').fadeOut('fast', function () {
        $('#boxpopup1').css('display', 'none');
        $('#boxpopup2').css('display', 'none');
        $('#boxpopup3').css('display', 'none');
        $('#boxpopup4').css('display', 'none');
        $('#boxpopup5').css('display', 'none');
        $('#boxpopup6').css('display', 'none');


    });
}


function closeOffersDialog(prospectElementID) {
    $(function ($) {
        $(document).ready(function () {
            $('#' + prospectElementID).css('position', 'absolute');
            $('#' + prospectElementID).animate({ 'left': '-100%' }, 500, function () {
                $('#' + prospectElementID).css('position', 'fixed');
                $('#' + prospectElementID).css('left', '100%');
                $('#overlay').fadeOut('fast');
                $('#boxpopup1').css('display', 'none');
                $('#boxpopup2').css('display', 'none');
                $('#boxpopup3').css('display', 'none');
                $('#boxpopup4').css('display', 'none');
                $('#boxpopup5').css('display', 'none');
                $('#boxpopup6').css('display', 'none');
            });
        });
    });
}




function open1() {
    $('#overlay').fadeIn('fast', function () {
        $('#boxpopup1').css('display', 'block');
        $('#boxpopup1').animate({ 'left': '15%' }, 500);
    });
}


function open2() {
    $('#overlay').fadeIn('fast', function () {
        $('#boxpopup2').css('display', 'block');
        $('#boxpopup2').animate({ 'left': '15%' }, 500);
    });
}


function open3() {
    $('#overlay').fadeIn('fast', function () {
        $('#boxpopup3').css('display', 'block');
        $('#boxpopup3').animate({ 'left': '15%' }, 500);
    });
}


function open4() {
    $('#overlay').fadeIn('fast', function () {
        $('#boxpopup4').css('display', 'block');
        $('#boxpopup4').animate({ 'left': '15%' }, 500);
    });
}


function open5() {
    $('#overlay').fadeIn('fast', function () {
        $('#boxpopup5').css('display', 'block');
        $('#boxpopup5').animate({ 'left': '15%' }, 500);
    });
}


function open6() {
    $('#overlay').fadeIn('fast', function () {
        $('#boxpopup6').css('display', 'block');
        $('#boxpopup6').animate({ 'left': '15%' }, 500);
    });
}
